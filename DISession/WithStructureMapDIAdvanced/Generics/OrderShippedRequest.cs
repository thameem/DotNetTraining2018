﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.Generics
{
    public class OrderShippedRequest
    {
        public int OrderId { get; set; }
        public int ShipmentType { get; set; }
    }
}

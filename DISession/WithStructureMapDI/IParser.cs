﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDI
{
    public interface IParser
    {
        ParserTypes ParserType { get; }
        string Parse(StorageOptions options);
    }
}

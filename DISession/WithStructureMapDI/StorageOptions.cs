﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDI
{
    public enum StorageTypes
    {
        FileSystem,
        Database,
        Azure
    }

    public enum ParserTypes
    {
        CommaSeperated,
        TabSeperated,
        ColonSeperated
    }

    public class StorageOptions
    {
        public StorageTypes StorageType { get; set; } = StorageTypes.FileSystem;
        public ParserTypes ParserType { get; set; } = ParserTypes.CommaSeperated;
        public string Path { get; set; }
    }
}

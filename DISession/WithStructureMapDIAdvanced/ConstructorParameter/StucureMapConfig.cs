﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.ConstructorParameter
{
    public static class StucureMapConfig
    {
        private static IContainer container = new Container();

        public static IContainer Container => container;

        public static void Configure()
        {
            container.Configure(x =>
            {
                x.For<IDocumentProvider>().Use<FileSystemDocumentProvider>().Ctor<string>("basePath").Is("Secret base path");
            });
        }
    }
}

﻿using LinqSession.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqSession.Operators
{
    public static class SetOperator
    {
        public static void Distinct()
        {
            using (var db = new Northwind())
            {
                var categoryNames = (from p in db.Products
                                     orderby p.Category.CategoryName descending
                                     select p.Category.CategoryName)
                                     .Distinct()
                                    .OrderByDescending(x => x);

                foreach (var item in categoryNames)
                {
                    Console.WriteLine("Name : {0}", item);
                }
            }
        }

        public static void Union()
        {
            using (var db = new Northwind())
            {

                var productFirstChars = from p in db.Products
                                        select p.ProductName;
                var customerFirstChars = from c in db.Customers
                                         select c.CompanyName;

                var uniqueFirstChars = productFirstChars.Union(customerFirstChars);

                foreach (var item in uniqueFirstChars)
                {
                    Console.WriteLine("Name : {0}", item);
                }
            }
        }

        public static void Intersect()
        {
            using (var db = new Northwind())
            {

                var productFirstChars = (from p in db.Products
                                         orderby p.ProductName
                                         select p.ProductName)
                                        .Take(10);
                var customerFirstChars = (from p in db.Products
                                          orderby p.ProductName
                                          select p.ProductName)
                                          .Skip(5)
                                        .Take(10);

                var commonFirstChars = productFirstChars.Intersect(customerFirstChars);

                foreach (var item in commonFirstChars)
                {
                    Console.WriteLine("Name : {0}", item);
                }
            }
        }

        public static void Exccept()
        {

            using (var db = new Northwind())
            {

                var productFirstChars = (from p in db.Products
                                         orderby p.ProductName
                                         select p.ProductName)
                                         .Take(10);
                var customerFirstChars = (from p in db.Products
                                          orderby p.ProductName
                                          select p.ProductName)
                                          .Skip(5)
                                        .Take(10);

                var productOnlyFirstChars = productFirstChars.Except(customerFirstChars);

                foreach (var item in productOnlyFirstChars)
                {
                    Console.WriteLine("Name : {0}", item);
                }
            }

        }
    }
}

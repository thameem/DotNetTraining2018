﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession.LSP
{
    public class BadCustomer
    {
        public virtual decimal GetDiscount()
        {
            return 5.0m;
        }

        public virtual void Add()
        {
            Console.WriteLine("I am saving data to DB");
            // save data to db
        }
    }
}

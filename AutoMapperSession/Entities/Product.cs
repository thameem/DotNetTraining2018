﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automapper2018.Entities
{
    public class Product
    {
        public decimal Price { get; set; }
        public string Name { get; set; }
    }
}

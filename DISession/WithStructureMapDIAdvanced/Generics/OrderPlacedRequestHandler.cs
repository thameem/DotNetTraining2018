﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.Generics
{
    public class OrderPlacedRequestHandler : IRequesHandler<OrderPlacedRequest>
    {
        public bool Handle(OrderPlacedRequest request)
        {
            Console.WriteLine("Order ID" + request.OrderId);
            Console.WriteLine("ProductId ID" + request.ProductId);
            return true;
        }
    }
}

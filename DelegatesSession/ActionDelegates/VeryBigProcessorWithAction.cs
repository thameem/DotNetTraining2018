﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesSession.ActionDelegates
{
    public class VeryBigProcessorWithAction
    {
        public Action<string> ProcessorLogger { get; set; }

        public void Process()
        {
            ProcessorLogger?.Invoke("Starting Process");

            for (int i = 0; i < 10000; i++)
            {
                ProcessorLogger?.Invoke("Processing " + i);
            }

            ProcessorLogger?.Invoke("Ending Process");
        }

        public void ProcessAdvanced(Action<string, int> logger)
        {
            logger?.Invoke("Starting Process Advanced", -1);

            for (int i = 0; i < 10000; i++)
            {
                logger?.Invoke("Processing " + i, i);
            }

            logger?.Invoke("Ending Process", -1);
        }
    }
}

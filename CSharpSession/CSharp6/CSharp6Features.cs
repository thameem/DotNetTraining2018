﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpNewFeatures.CSharp6
{
    public class CSharp6Features
    {
        public static void FirstOrDefaultWithOutNullPropogationOperator()
        {
            var persons = new List<Person>();

            for (int i = 1; i <= 10; i++)
            {
                var person = new Person();
                person.Id = i;
                person.FirstName = "First Name " + i.ToString();
                persons.Add(person);
            }

            var firstPerson = persons.FirstOrDefault(x => x.Id == 1);
            Console.WriteLine("First Person Name : " + firstPerson.FirstName);

            var leventhPerson = persons.FirstOrDefault(x => x.Id == 11);
            if (leventhPerson != null)
            {
                Console.WriteLine("Leventh Person Name : " + leventhPerson.FirstName);
            }
        }

        public static void FirstOrDefaultWithNullPropogationOperator()
        {
            var persons = new List<Person>();

            for (int i = 1; i <= 10; i++)
            {
                var person = new Person();
                person.Id = i;
                person.FirstName = "First Name " + i.ToString();
                persons.Add(person);
            }

            var firstPerson = persons.FirstOrDefault(x => x.Id == 1);
            Console.WriteLine("First Person Name : " + firstPerson.FirstName);

            var leventhPerson = persons.FirstOrDefault(x => x.Id == 11);

            Console.WriteLine("Leventh Person Name : " + leventhPerson?.FirstName);

            Console.WriteLine("Leventh Person Name : " + leventhPerson?.FirstName ?? "Unknown");
            // order?.Customer?.Address?.PostalCode ?? "Unknown Postal Code"
        }

        public static void StringConcatWithoutStringInteropolation()
        {
            var prefix = "Mr";
            var concat = prefix + ".Some value";

            var format = string.Format("{0}.Some Value", prefix);
        }


        public static void StringConcatWithStringInteropolation()
        {
            var prefix = "Mr";
            var interopolation = $"{prefix + "possible"} what is this{prefix}.Some value";

            var isMale = true;

            var condtional = $"{(isMale ? "Mr" : "Ms")}. Some Value";
        }

        public static void NameOfOperator()
        {
            var personWithoutName = new PersonNew();
            Console.WriteLine(personWithoutName.GetDisplay());

            var personWithName = new PersonNew
            {
                FirstName = "Some Name"
            };

            Console.WriteLine(personWithName.GetDisplay());

            //Console.WriteLine("Calling Method SetName");
            Console.WriteLine("Calling Method" + nameof(PersonNew.SetName));


        }

        public static void DictionaryWithIndexIntializers()
        {
            var dictionary = new Dictionary<int, string>()
            {
                { 1, "c#"},
                {2, "VB" },
                { 3, "SQL"}
            };

            var dictionaryWithIndexInitializers = new Dictionary<int, string>
            {
                [1] = "C#",
                [2] = "VB",
                [3] = "SQL"
            };
        }

        public static void ExceptionFilter()
        {
            var shouldThrow = false;
            try
            {
                throw new NullReferenceException();
            }
            catch (NullReferenceException ex) when (Log(ex))
            {
                //Log(ex);
                Console.Write(ex.Message);
            }
            catch (Exception ex) when (shouldThrow)
            {
                throw;
            }
        }

        public static bool Log(Exception ex)
        {
            Console.Write(ex.Message);

            return false;
        }
    }

    //public class SampleClassCSharp1
    //{
    //    private string _firstName;
    //    public string FirstName
    //    {
    //        get
    //        {
    //            return _firstName;
    //        }

    //        set
    //        {
    //            _firstName = value;
    //        }
    //    }
    //}

    //public class SampleClassCSharp2
    //{
    //    public string FirstName { get; set; }        
    //}  

    // Initializers for auto-properties 
    public class SampleClass
    {
        public SampleClass()
        {
            this.FirstName = this.IsMale ? "Mr. Unknown" : "Ms. Unknown";
        }

        public bool IsMale { get; set; }
        public string FirstName { get; set; }
    }


    public class SampleClassCShar6
    {
        public string FirstName { get; set; } = "Unknown";
    }

    // Initializers for auto-properties for getter only properties
    public class SampleClass2
    {
        public string TypeOfOrder { get; } = "Online";
    }


    // Expression embodied members
    public class SampleClassWithoutExpressionEmbodiedMemebers
    {
        public string GetValue()
        {
            return "I am returning a value";
        }

        public string GetValueWithParam(string prefix)
        {
            return prefix + " " + "I am returning value";
        }

        public string IsInitialized
        {
            get { return "Yes"; }
        }

        private string _isWeb;
        public string IsWeb
        {
            get
            {
                return _isWeb;
            }

            set
            {
                _isWeb = value;
            }
        }
    }

    public class SampleClassWithExpressionEmbodiedMemebers
    {
        public string GetValue => "I am returning a value";

        public string GetValueWithParam(string prefix) => prefix + " " + "I am returning value";

        public string IsInitialized => "Yes";

        private string _isWeb;
        public string IsWeb
        {
            get => _isWeb;
            set => _isWeb = value;
        }
    }

    public class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
    }

    public class PersonNew
    {
        public int Id { get; set; }
        public string FirstName { get; set; }

        public string GetDisplay()
        {
            if (string.IsNullOrWhiteSpace(this.FirstName))
            {
                //return "FirstName is Null or empty";
                return $"{nameof(FirstName)} is null or empty";
            }

            return $"{nameof(FirstName)} is {this.FirstName}";
        }

        public void SetName(string firstName)
        {
            if (string.IsNullOrEmpty(firstName))
            {
                // Console.WriteLine($"firstName is required");
                Console.WriteLine($"{nameof(firstName)} is required");
            }

            this.FirstName = firstName;
        }
    }
}

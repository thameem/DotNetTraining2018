﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession.LSP
{
    public interface IHaveDiscount
    {
        decimal GetDiscount();
    }
}

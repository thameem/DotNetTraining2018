﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.CircularDependencyServiceLocater
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly ISecurityService securityService;

        public AuthenticationService(ISecurityService securityService)
        {
            this.securityService = securityService;
        }

        public bool IsSessionValid(string userName)
        {
            return true;
        }
    }
}

﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.Generics
{
    public class StructureMapConfig
    {
        private static IContainer container = new Container();

        public static IContainer Container => container;

        public static void Configure()
        {
            container.Configure(x =>
            {
                //x.For<IRequesHandler<OrderPlacedRequest>>().Use<OrderPlacedRequestHandler>();
                //x.For<IRequesHandler<OrderShippedRequest>>().Use<OrderShippedRequestHandler>();

                x.Scan(cfg =>
                {
                    cfg.TheCallingAssembly();
                    cfg.ConnectImplementationsToTypesClosing(typeof(IRequesHandler<>));
                });
            });
        }
    }
}

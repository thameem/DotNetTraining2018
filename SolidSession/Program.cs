﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession
{
    class Program
    {
        static void Main(string[] args)
        {
            // LSP

            //var badCustomers = new List<SOLIDSession.LSP.BadCustomer>();

            //badCustomers.Add(new LSP.BadCustomer());
            //badCustomers.Add(new LSP.BadCustomer());
            //badCustomers.Add(new LSP.BadEnquiry());

            //foreach (var item in badCustomers)
            //{
            //    item.Add();
            //}

            var customers = new List<SOLIDSession.LSP.ISaveData>();

            customers.Add(new LSP.Customer());
            customers.Add(new LSP.Customer());
            //customers.Add(new LSP.Enquiry()); - it will not compile, just sample

            foreach (var item in customers)
            {
                item.Add();
            }

            var disountcustomer = new List<SOLIDSession.LSP.IHaveDiscount>();

            disountcustomer.Add(new LSP.Customer());
            disountcustomer.Add(new LSP.Customer());
            disountcustomer.Add(new LSP.Enquiry());

            foreach (var item in disountcustomer)
            {
                Console.WriteLine("Discount " + item.GetDiscount());
            }

            // LSP END

            // DIP
            var useFileLogger = true;
            DIP.ILogger logger = null;

            if (useFileLogger)
            {
                logger = new DIP.FileLogger();
            }
            else
            {
                logger = new DIP.DBLogger();
            }

            var customer = new SOLIDSession.DIP.Customer(logger);

            Console.ReadLine();
        }
    }
}

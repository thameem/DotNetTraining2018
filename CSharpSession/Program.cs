﻿using CSharpNewFeatures.CSharp6;
using CSharpNewFeatures.CSharp7;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpNewFeatures
{
    class Program
    {
        static void Main(string[] args)
        {
            //CSharp6Features.FirstOrDefaultWithOutNullPropogationOperator();
            //CSharp6Features.FirstOrDefaultWithNullPropogationOperator();
            //CSharp6Features.NameOfOperator();
            //CSharp6Features.ExceptionFilter();

            //InlineOutVarible.OutVariable();
            //StringLiterals.WithoutLiterals();
            //StringLiterals.WithLiterals();

            ValuesTuples.ValueTuplesAssigning();
            Console.ReadLine();
        }
    }
}

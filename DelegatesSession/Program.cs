﻿using DelegatesSession.FuncDelegates;
using DelegatesSession.WithoutDelegates;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesSession
{
    class Program
    {
        static void Main(string[] args)
        {
            //var processor = new VeryBigProcessor();
            //processor.Process();

            //var processor = new Delegates.VeryBigProcesor();
            //processor.ProcessorLogger = new Delegates.Logger(ConsoleLogger);
            //processor.Process();

            //var processor = new Delegates.VeryBigProcesor();
            //processor.ProcessWithParameter(new Delegates.Logger(FileLogger));
            //processor.ProcessWithParameter(null);

            //var processor = new ActionDelegates.VeryBigProcessorWithAction();
            //processor.ProcessorLogger = ConsoleLogger;
            //processor.Process();

            var processor = new ActionDelegates.VeryBigProcessorWithAction();
            //processor.ProcessorLogger = (message) =>
            //{
            //    Console.WriteLine("I am from anonymous method " + message);
            //};

            //processor.Process();

            //processor.ProcessAdvanced((message, value) => { Console.WriteLine("I am mesage - {0} with value {1}", message, value); });

            //Action<string, int> logger = (message, value) => {
            //    Console.WriteLine("I am mesage - {0} with value {1} from Variable", message, value);
            //};

            //processor.ProcessAdvanced(logger);

            //var orderProcessor = new OrderProcessor();
            //orderProcessor.Process(ValueCalculator);

            Func<int, int> calculator = (input) =>
            {
                return input * 10;
            };

            var orderProcessor = new OrderProcessor();
            orderProcessor.Process(calculator);
            Console.ReadLine();

            using (var connection = new SqlConnection(""))
            {
                connection.Open();
                connection.Close();
            }

            
        }

        private static void ConsoleLogger(string logMessage)
        {
            Console.WriteLine(logMessage);
        }

        private static void FileLogger(string message)
        {
            Console.WriteLine("I am writing to file " + message);
        }

        private static int ValueCalculator(int input)
        {
            return input * 10;
        }
    }
}

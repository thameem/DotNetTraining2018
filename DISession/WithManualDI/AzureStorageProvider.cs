﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithManualDI
{
    public class AzureStorageProvider : IStorageProvider
    {
        public StorageTypes StorageType => StorageTypes.Azure;

        public string GetContent(StorageOptions options)
        {
            return $"I am returning data from Azure with path - {options.Path}";
        }
    }
}

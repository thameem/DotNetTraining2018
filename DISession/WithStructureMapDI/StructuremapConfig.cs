﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDI
{
    public static class StructuremapConfig
    {
        private static IContainer container = new Container();

        public static IContainer Container => container;

        public static void Configure()
        {
            container.Configure(x => {
                x.For<IParser>().Use<CommaSeperatedParser>().Named(ParserTypes.CommaSeperated.ToString());
                x.For<IParser>().Use<ColonSperatedParser>().Named(ParserTypes.ColonSeperated.ToString()); 
                x.For<IParser>().Use<TabSperatedParser>().Named(ParserTypes.TabSeperated.ToString()); 
                x.For<IStorageProvider>().Use<FileSystemStorageProvider>();
                x.For<IStorageProvider>().Use<DatabaseStorageProvider>();
                x.For<IStorageProvider>().Use<AzureStorageProvider>();
            });
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced
{
    public interface IApplicationConfig
    {
        string ConnectionString { get; set; }
    }
}

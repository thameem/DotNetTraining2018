﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession.OCP
{
    public class GoldCustomer : BaseCustomer
    {
        public override decimal GetDiscount()
        {
            return 7.5m;
        }
    }
}

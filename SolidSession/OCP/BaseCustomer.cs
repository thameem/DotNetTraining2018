﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession.OCP
{
    public class BaseCustomer
    {
        public virtual decimal GetDiscount()
        {
            return 5.0m;
        }
    }
}

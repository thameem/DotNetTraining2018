﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.Generics
{
    public class OrderPlacedRequest
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
    }
}

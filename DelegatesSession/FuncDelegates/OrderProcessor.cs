﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesSession.FuncDelegates
{
    public class OrderProcessor
    {        
        public void Process(Func<int, int> calculator)
        {
            for (int i = 0; i < 1000; i++)
            {
                var calculatedValue = calculator(i);

                Console.WriteLine("calculated value from calling side " + calculatedValue);
            }
        }
    }
}

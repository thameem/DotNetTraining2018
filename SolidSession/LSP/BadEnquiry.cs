﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession.LSP
{
    public class BadEnquiry : BadCustomer
    {
        public override decimal GetDiscount()
        {
            return 0.0m;
        }

        public override void Add()
        {
            throw new InvalidOperationException("Not valid");
        }
    }
}

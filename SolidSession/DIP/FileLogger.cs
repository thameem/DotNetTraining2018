﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession.DIP
{
    public class FileLogger : ILogger
    {
        public void Log(Exception exception)
        {
            Console.WriteLine("I write log to file " + exception.Message);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession.SRP
{
    public class BadCustomer
    {
        public void Add()
        {
            try
            {
                // Some code to save customer to db
            }
            catch(Exception ex)
            {
                File.AppendAllText("Log.txt", ex.Message);
            }
        }

        public void Update()
        {
            try
            {
                // Some code to save customer to db
            }
            catch (Exception ex)
            {
                File.AppendAllText("Log.txt", ex.Message);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDI
{
    public class DatabaseStorageProvider : IStorageProvider
    {
        public StorageTypes StorageType => StorageTypes.Database;

        public string GetContent(StorageOptions options)
        {
            return $"I am returning data from Database with path - {options.Path}";
        }
    }
}

﻿using AutoMapper;
using Automapper2018.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Automapper2018.Topics
{
    public class AutoMapperAdvanced
    {
        public static void Validation()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ValidateSource, ValidateDestination>(MemberList.None);
            });

#if DEBUG
            Mapper.Configuration.AssertConfigurationIsValid();
#endif
            var source = new ValidateSource();
            source.Value = DateTime.Now;

            var destination = Mapper.Map<ValidateDestination>(source);

            Console.Write("Value " + destination.Value);
        }

        public static void NullSubstitution()
        {
            Mapper.Initialize(cfg => cfg.CreateMap<NullSubstituteSource, NullSubstituteDestination>().ForMember(x => x.Value, y => y.NullSubstitute("N/A")));

            var source = new NullSubstituteSource();
            source.Value = "Some Value";

            var destination = Mapper.Map<NullSubstituteDestination>(source);

            Console.Write("Value " + destination.Value);
        }

        public static void ConstructUsing1()
        {

            Mapper.Initialize(cfg => cfg.CreateMap<ConstructorSource, ConstructorDestination>());

            var source = new ConstructorSource();
            source.Value = "Some Value";

            var destination = Mapper.Map<ConstructorDestination>(source);

            Console.WriteLine("Value " + destination.Value);
        }


        public static void ConstructUsing2()
        {

            Mapper.Initialize(cfg => cfg.CreateMap<ConstructorSource, ConstructorDestination2>()
                                        .ConstructUsing((s) => new ConstructorDestination2(new Logger())));


            //Mapper.Initialize(cfg => cfg.CreateMap<ConstructorSource, ConstructorDestination2>()
            //                            .ConstructUsing((s) => {
            //                                if (s.Value == "Some Value")
            //                                {
            //                                    return null;
            //                                }

            //                                return new ConstructorDestination2(new Logger());
            //                            }));

            var source = new ConstructorSource();
            source.Value = "Some Value";

            var destination = Mapper.Map<ConstructorDestination2>(source);

            Console.WriteLine("Value " + destination.Value);
        }

        public static void TypeConvertor()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<string, int>().ConvertUsing((v) => Convert.ToInt32(v));
                cfg.CreateMap<string, DateTime>().ConvertUsing<DateTimeConvertor>();
                cfg.CreateMap<string, Type>().ConvertUsing(new StringTypeConvertor());
                cfg.CreateMap<ConverterSource, ConverterDestination>();
            });


            var source = new ConverterSource();
            source.Value1 = "1001";
            source.Value2 = "02/10/2018";
            source.Value3 = typeof(ValidateSource).FullName;

            var destination = Mapper.Map<ConverterDestination>(source);

            Console.WriteLine("Value 1: " + destination.Value1);
            Console.WriteLine("Value 2: " + destination.Value2);
            Console.WriteLine("Value 3: " + destination.Value3.Name);
        }

        public static void BeforeAfterMap1()
        {
            Mapper.Initialize(cfg => cfg.CreateMap<BeforeAfterMapSource, BeforeAfterMapDestination>());

            var source = new BeforeAfterMapSource();
            source.Id = Guid.NewGuid();
            source.Value = "Some Value";

            source.Log();

            var destination = Mapper.Map<BeforeAfterMapDestination>(source);

            destination.Log();


        }

        public static void BeforeAfterMap2()
        {
            Mapper.Initialize(cfg => cfg.CreateMap<BeforeAfterMapSource, BeforeAfterMapDestination>()
                                        .BeforeMap((s, d) =>
                                        {
                                            s.Log();
                                        })
                                        .AfterMap((s, d) => d.Log()));

            var source = new BeforeAfterMapSource();
            source.Id = Guid.NewGuid();
            source.Value = "Some Value";

            var destination = Mapper.Map<BeforeAfterMapDestination>(source);
        }

        public static void MappingConfigurationSplit()
        {
            Mapper.Initialize(cfg => {
                cfg.AddProfile<OrderMappings>();
                cfg.AddProfile<EmployeeMappings>();
            });
        }

        private class ValidateSource
        {
            public DateTime Value { get; set; }
        }

        private class ValidateDestination
        {
            public decimal Value { get; set; }
            public string AnotherValue { get; set; }
        }

        private class NullSubstituteSource
        {
            public string Value { get; set; }
        }

        private class NullSubstituteDestination
        {
            public string Value { get; set; }
        }

        public class Logger
        {
            public void Info(string mesage)
            {
                Console.WriteLine("Info " + mesage);
            }
        }

        public class ConstructorSource
        {
            public string Value { get; set; }
        }

        public class ConstructorDestination
        {
            private readonly Logger logger;

            public ConstructorDestination() : this(new Logger())
            {

            }

            public ConstructorDestination(Logger logger)
            {
                this.logger = logger;
            }

            public string Value { get; set; }
        }

        public class ConstructorDestination2
        {
            private readonly Logger logger;

            public ConstructorDestination2(Logger logger)
            {
                this.logger = logger;
            }

            public string Value { get; set; }
        }

        public class ConverterSource
        {
            public string Value1 { get; set; }
            public string Value2 { get; set; }
            public string Value3 { get; set; }
        }

        public class ConverterDestination
        {
            public int Value1 { get; set; }
            public DateTime Value2 { get; set; }
            public Type Value3 { get; set; }
        }

        public class DateTimeConvertor : ITypeConverter<string, DateTime>
        {
            public DateTime Convert(string source, DateTime destination, ResolutionContext context)
            {
                return System.Convert.ToDateTime(source);
            }
        }

        public class StringTypeConvertor : ITypeConverter<string, Type>
        {
            public Type Convert(string source, Type destination, ResolutionContext context)
            {
                return Assembly.GetExecutingAssembly().GetType(source);
            }
        }

        public class BeforeAfterMapSource
        {
            public Guid Id { get; set; }
            public string Value { get; set; }

            public void Log()
            {
                Console.WriteLine("Source ID:" + this.Id);
                Console.WriteLine("Source Value:" + this.Value);
            }
        }

        public class BeforeAfterMapDestination
        {
            public Guid Id { get; set; }
            public string Value { get; set; }

            public void Log()
            {
                Console.WriteLine("Destination ID:" + this.Id);
                Console.WriteLine("Destination Value:" + this.Value);
            }
        }

        public class OrderMappings : Profile
        {
            public OrderMappings()
            {
                CreateMap<Order, OrderDTO>();
            }            
        }

        public class EmployeeMappings : Profile
        {
            public EmployeeMappings()
            {
                CreateMap<Person, PersonDTO>();
            }           
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced
{
    public class ApplicationConfig : IApplicationConfig
    {
        public string ConnectionString { get; set; }
    }
}

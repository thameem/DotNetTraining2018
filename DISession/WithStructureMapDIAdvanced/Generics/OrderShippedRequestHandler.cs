﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.Generics
{
    public class OrderShippedRequestHandler : IRequesHandler<OrderShippedRequest>
    {
        public bool Handle(OrderShippedRequest request)
        {
            Console.WriteLine("Order ID" + request.OrderId);
            Console.WriteLine("Shipment ID" + request.ShipmentType);
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpNewFeatures.CSharp7
{
    public class StringLiterals
    {
        public static void WithoutLiterals()
        {
            int value = 2147483647;

            Console.WriteLine(value);
        }

        public static void WithLiterals()
        {
            int value = 2_147_483_647 - 20;

            Console.WriteLine(value);
        }
    }
}

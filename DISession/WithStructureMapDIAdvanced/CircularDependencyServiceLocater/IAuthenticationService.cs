﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.CircularDependencyServiceLocater
{
    public interface IAuthenticationService
    {
        bool IsSessionValid(string userName);
    }
}

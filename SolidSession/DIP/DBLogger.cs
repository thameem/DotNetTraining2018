﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession.DIP
{
    public class DBLogger : ILogger
    {
        public void Log(Exception exception)
        {
            Console.WriteLine("I write log to db " + exception.Message);
        }
    }
}
